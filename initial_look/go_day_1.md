credit :https://hackernoon.com/basics-of-golang-for-beginners-6bd9b40d79ae 
# Function
Functions have the general for ` func function_name( [parameter list] ) [ return_types]
{
	body of the function
] `

# Looping (` for ` loop)
Go has only one looping construct, the for loop. The basic syntax is
` for [condtion | (init; condtion; increment ) | Range]
{
	statements(s);
} `
* the init statement: executed before the first iteration
* the condition expression: evaluated before every iteration
* the post statement: executed at the end of every iteration

# Array
Arrays store a fixed-size sequential collection of elements of the same type. (can think of it as a collection of variables of the same type)
Syntax of an array:
` var variable_name [SIZE] variable_type `
so ` var balance [10] float32 ` is a 10 element array called balance which has type float32

